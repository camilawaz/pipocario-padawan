<?php
/*
* Template name: Resposta Teste 2
*
*/
?>        

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Resposta Teste 2</title>

        <!-- Bootstrap core CSS -->
        <link href="<?php bloginfo('template_url');?>/assets/css/bootstrap.css" rel="stylesheet">
        
        <!-- Custom styles for this template -->
        <link href="<?php bloginfo('template_url');?>/assets/css/teste-2.css" rel="stylesheet">

        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
        <?php wp_head(); ?>
    </head>
    <body>
        
        <div class="container">
            <header></header>

            <section id="featured"></section>
            
            <section id="posts">
                <article class="post"> 
                    <div class="img-post blue"></div>
                    <p class="txt-post">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse viverra finibus lacinia. Vivamus egestas molestie turpis at porttitor. Suspendisse vitae sagittis eros, pulvinar finibus diam. Fusce eu tellus ac magna molestie sodales.</p>
                </article>
                <article class="post"> 
                    <div class="img-post green"></div>
                    <p class="txt-post">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse viverra finibus lacinia. Vivamus egestas molestie turpis at porttitor. Suspendisse vitae sagittis eros, pulvinar finibus diam. Fusce eu tellus ac magna molestie sodales.</p>
                </article>
                <article class="post"> 
                    <div class="img-post orange"></div>
                    <p class="txt-post">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse viverra finibus lacinia. Vivamus egestas molestie turpis at porttitor. Suspendisse vitae sagittis eros, pulvinar finibus diam. Fusce eu tellus ac magna molestie sodales.</p>
                </article>
            </section>

        </div>
    </body>
</html>
