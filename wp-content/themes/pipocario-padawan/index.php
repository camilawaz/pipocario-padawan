<?php get_header();?>


	<!-- +++++ Welcome Section +++++ -->
	<div id="ww">
	    <div class="container">
			<div class="row">
				<div class="col-lg-8 col-lg-offset-2 centered">
					<img src="<?php bloginfo('template_url');?>/assets/img/pipoca-padawan.png" width="200px"alt="Seja Padawan">
					<h1>Olá, Pipocário Padawan</h1>
					<p>Primeiro queremos agradecer pela confiança e pelo tempo dedicado em fazer parte dessa entrevista. Depois de batermos um papo e alinharmos as expectativas, chegou a hora de meter a mão na massa e fazer o que mais amamos, codar! :)</p>
					<p>Acima no menu, você encontra 3 testes. Pedimos que você siga a ordem do menu. Todo o teste será acompanhado pela equipe da Pipoca via Skype.</p>
					<h3>Boa Sorte!!!</h3>
					
				
				</div><!-- /col-lg-8 -->
			</div><!-- /row -->
	    </div> <!-- /container -->
	</div><!-- /ww -->
	
<?php get_footer(); ?>


