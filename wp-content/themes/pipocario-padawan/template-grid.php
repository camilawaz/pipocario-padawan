<?php
/*
* Template name: Exercício Grid
*
*/

get_header();?>




        

<div class="container pt">
  <div class="row mt centered">
    <div class="col-lg-12" >
      <h1> Exercício WP </h1>
    </div>
      
      
        <!-- Loop de posts -->
        <?php $posts = new WP_Query('showposts'); ?>
        <?php while( $posts->have_posts() ) : $posts->the_post(); ?>

            <div class="col-lg-4" >
                <article class="post" style="background-image: url(<?php the_post_thumbnail_url('medium'); ?>)">
                    <a class="green" href="<?php the_permalink(); ?>">
                        <p><?php the_title(); ?></p>
                    </a>
                </article>
            </div>

        <?php endwhile; ?>

  </div><!-- /row -->
</div><!-- /container -->

<?php get_footer();?>
